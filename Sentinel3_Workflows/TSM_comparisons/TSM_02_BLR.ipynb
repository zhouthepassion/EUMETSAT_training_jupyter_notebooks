{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to the TSM worflow exercise!\n",
    "\n",
    "### Part II: Apply BLR (Baseline Residual) atmospheric correction to OLCI images\n",
    "\n",
    "This .ipynb file corresponds to Part II, where you will:\n",
    "\n",
    "Learn the different steps and how to apply a simple atmospheric correction specifically designed for OLCI at sediment-dominated turbid waters (Río de la Plata and Estuario do Tejo) based on Base Line Residuals (BLR).\n",
    "\n",
    "More detailed description of the BLR atmospheric correction can be found in:\n",
    "\n",
    "\n",
    "*Gossn, J.I., K.G. Ruddick and A.I. Dogliotti (2019) Atmospheric Correction of OLCI Imagery over Extremely Turbid Waters Based on the Red, NIR and 1016 nm Bands and a New Baseline Residual Technique, Remote Sensing, 2072-4292, vol. 11-3, 220, doi:10.3390/rs11030220, http://www.mdpi.com/2072-4292/11/3/220*\n",
    "\n",
    "The water reflectance images you will obtain by applying this algorithm will be used to compute satellite-derived TSM values at Part III, which will be compared with other TSM satellite products as well as with *in situ* TSM values.\n",
    "\n",
    "\n",
    "\n",
    "#### WARNING: To execute this script you should have executed part I at both regions TAGUS and RDP."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is an *atmospheric correction*??\n",
    "In optical imagery, either from land or water targets, the atmosphere that stands between the target of interest and the satellite has a non negligible contribution on the radiance budget that arrives at the sensor. To remove this contribution, it is necessary to design an *atmospheric correction* algorithm. \n",
    "\n",
    "The latter was a very brief explanation to refresh what you saw at the lectures. You can internalize more on the topic by reading:\n",
    "\n",
    "#### To start:\n",
    "\n",
    "*GSP216, Introduction to Remote Sensing: Radiometric Corrections. Humboldt State University. Humboldt Geospatial Online http://gsp.humboldt.edu/olm_2015/Courses/GSP_216_Online/lesson4-1/radiometric.html*\n",
    "\n",
    "#### Advanced (mainly Ocean Color):\n",
    "\n",
    "*Mobley, C.; Werdell, J.; Franz, B.; Ahmad, Z.; Bailey, S. Atmospheric Correction for Satellite Ocean Color Radiometry; Technical report; NASA Goddard Space Flight Center: Greenbelt, MD, USA, 2016.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### BLR scheme: Refreshing what you saw at the lectures\n",
    "Just to refresh what will be seen at the lectures, the BLR-AC is based on spectral magnitudes called here \"baseline residuals\", which are quasi-invariant under atmospheric conditions. These are defined by means of triplets of bands as Fig. 1 shows [Figure 2 @ Gossn *et al.* 2019]:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"blrExamples.png\" style=\"width:900px;height:450px;\">\n",
    "\n",
    "<caption><center> <u>Figure 1 [Fig. 2 G19]</u>:\n",
    "(a) RGB Composite of OLCI-A image on the Río de la Plata, OLCI-A 2017-10-31T12:47:23Z, using Rayleigh-corrected (RC) reflectances at 620 nm (R) 560 nm (G) and 442 nm (B). (b) Rayleigh-corrected reflectances of the red, near-infrared, short-wave-infra-red (RNS) bands used for the baseline residual atmospheric correction (BLR-AC) approach at the sites A, B and C, together with the BLR values (vertical black solid lines). Notice that BLR values approach to 0 at site C (i.e. quasi-linear RC reflectance), where water relfectance is close to 0 in the RNS. <br> </center></caption>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The BLR-AC requires a small amount of simple steps to be computed, which you'll be performing throughout this script. They are presented in this scheme:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "<img src=\"blrScheme.png\" style=\"width:600px;height:400px;\">\n",
    "\n",
    "<caption><center> <u>Figure 2 [Fig. 7 G19]</u>: Different steps to perform \"BLR\" atmospheric correction. This algorithm is designed to estimate water reflectance just above the surface, $\\rho_{w}$. <br> </center></caption>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We won't be computing the first step of this chain since it requires the use of another software like the SeaDAS software, or SNAP. It consists of a Rayleigh correction, i.e., the subtraction of the molecular scattering caused by air molecules in the atmosphere such as $O_{2}$ and $N_{2}$ (also described in Part I).\n",
    "\n",
    "Let's get started with the rest of the steps... "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import modules:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import os\n",
    "from netCDF4 import Dataset\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.interpolate import griddata\n",
    "from sklearn.neighbors import NearestNeighbors\n",
    "\n",
    "import roiNcdf as rn # This last module is home-made for the purpose of this course. Its defined apart because the functions that are defined inside it are used in several ocassions at the three main scripts, TSM_01, 02 and 03. These functions are defined at roinNcdf.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define functions:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Functions: Determine nearest neighbor corresponding to BLR calibration grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def nearest_neighbors(pixelBLRs, calBLRs, nbr_neighbors):\n",
    "    '''\n",
    "    This function will return which BLR triplet from the set \"calBLRs\" is the closest to each of the BLR triplets \n",
    "    in pixlBLRs.\n",
    "    '''\n",
    "    nn = NearestNeighbors(nbr_neighbors, metric='euclidean', algorithm='brute').fit(calBLRs)\n",
    "    dists, idxs = nn.kneighbors(pixelBLRs)\n",
    "    KNN = {'dists':dists,'idxs':idxs}\n",
    "    return KNN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Functions: Compute Rayleigh transmittance factor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def tRay(mu,tau,bb,band):\n",
    "    '''\n",
    "    Compute transmittance factor with the following model: T = exp(-bb.tau.mu), where\n",
    "    mu  = air mass factor\n",
    "    bb  = backscattering of the atmospheric species at band \"band\"\n",
    "    tau = optical thickness of the atmospheric species at band \"band\"\n",
    "    \n",
    "    (For the moment, only corrects for one component, i.e. Rayleigh Scattering)\n",
    "    '''\n",
    "    if tau.ndim == 1 and np.size(bb) == 1:\n",
    "        tRay = (np.exp(-mu)**(bb*tau[band]))\n",
    "    else:\n",
    "        pass # for the moment...\n",
    "    return tRay"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Main variable inputs for processing: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# e.g. MYPATH = os.path.join(\"C:/\",\"Users\",\"me\",\"Desktop\")\n",
    "MYPATH = \"<please insert your path from Data_Path_Checker.ipynb here, removing the quotes and chevrons>\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select region\n",
    "region = 'TAGUS'\n",
    "\n",
    "# Load stored ROI (computed in Part I for both TAGUS and RDP)\n",
    "boxRoi = np.load(os.path.join(MYPATH,'S3_workflow_data',region,'boxRoi.npy')).item()\n",
    "\n",
    "# Compute absolute paths to L1 data (CODA) and RC data (CODA>>SeaDAS)\n",
    "pathL1 = rn.findFile(os.path.join(MYPATH,'S3_workflow_data',region),'S3A_OL_1_EFR*.SEN3')\n",
    "\n",
    "pathRC = rn.findFile(os.path.join(MYPATH,'S3_workflow_data',region),'S3A_DERIVED*.SEN3')\n",
    "\n",
    "print(pathRC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Main fix inputs for processing (do not modify them) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mode 'tCorr' means we will apply an \"equivalent transmittance factor\" to the computed BLRs.\n",
    "mode = 'tCorr'\n",
    "\n",
    "# These are the bands we currently use to perform the AC on OLCI (620, 709, 779, 865 and 1016 nm).\n",
    "blrBands     = np.array([6,10,15,16,20])\n",
    "\n",
    "# These are the parameters to perform the epsilon correction step:\n",
    "blrCorrBands = {'ctor':20,'cted':16} # The \"corrector\" (1016nm) and \"corrected\" (865nm) bands \n",
    "epsRange     = np.array([0.85,1.25]) # rhoa(cted)/rhoa(ctor)\n",
    "\n",
    "# The bands that will be corrected using this scheme.\n",
    "rhowBands    = np.array([6,7,10,15,16,20])\n",
    "\n",
    "# BLR calibration dataset\n",
    "calDS     = pd.read_csv(os.path.join('blrAncillary','blrCalDS'))\n",
    "\n",
    "# List of mean OLCI wavelengths (in integer format):\n",
    "olciWave = [int(wave) for wave in list(open(os.path.join('blrAncillary','olciBandsWave'),'r'))]\n",
    "\n",
    "# List of Rayleigh optical thicknesses to compute transmittances:\n",
    "tauRay   = np.array([float(tau)  for tau  in list(open(os.path.join('blrAncillary','rayleigh_olci'),'r'))])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extract subset of lat/lon/radiance values in track geometry (row-col) according to selected ROI"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geo=Dataset(os.path.join(pathL1,'geo_coordinates.nc'),'r') # Read netCDF file\n",
    "lat0 = geo.variables['latitude'] [:,:]\n",
    "lon0 = geo.variables['longitude'][:,:]\n",
    "\n",
    "ROI = rn.selectROI(lat0,lon0,boxRoi)\n",
    "\n",
    "lat = rn.sliceFromROI(lat0,ROI)\n",
    "lon = rn.sliceFromROI(lon0,ROI)\n",
    "\n",
    "# This will be the \"caché\" (a Python dictionary) where all the main steps will be stored\n",
    "im = {'lat':lat,'lon':lon,'ROI':ROI}\n",
    "\n",
    "rcSeadas = Dataset(os.path.join(pathRC,'l2genRcRhoS'),'r')\n",
    "rcVar = ['rhos_' + str(olciWave[o]) for o in range(len(olciWave)) if o in rhowBands] + ['l2_flags']\n",
    "for v in rcVar:\n",
    "    print(v)\n",
    "    v0 = v\n",
    "    if v0 == 'rhos_1016':\n",
    "        v0 = 'rhos_1012'\n",
    "    matrix = rcSeadas['geophysical_data'][v0][:,:]\n",
    "    matrixRoi = rn.sliceFromROI(matrix,ROI)\n",
    "    im[v] = matrixRoi\n",
    "print(im)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute air mass factor $\\mu$\n",
    "\n",
    "The air mass factor measures how many *atmospheric heights* will the radiation need to cross through in order to:\n",
    "\n",
    "i) Get from TOA to the water interface without being scattered in its path;\n",
    "ii) Being reflected at the water interface, and\n",
    "iii) Arriving from the Earth's surface to the sensor without being scattered in its path.\n",
    "\n",
    "Its simplest mathematical expression is: $\\mu = \\frac{1}{cos(\\theta_{S})} + \\frac{1}{cos(\\theta_{O})}$, where $\\theta_{S}$ and $\\theta_{O}$ are the solar and observing zenith angles. Notice that it is minimized at $\\mu = 2$ when both Sun and sensor are at zenith ($\\theta_{S}=\\theta_{O}=0$), meaning that the photon will need to cross the atmosphere exactly twice in a vertical direction.\n",
    "\n",
    "We will compute $\\mu$, since it is needed to compute atmospheric transmittances.\n",
    "\n",
    "For OLCI standard L1B products, SZA ($\\theta_{S}$) and OZA ($\\theta_{O}$) are given only at \"tie\" pixels, i.e., not for every pixel inside the image. Since SZA and OZA vary smoothly in space, it is reasonable to perform an interpolation to find their values at pixels which are not tie points. Once this is done, we have the full SZA and OZA matrices and we are able to compute the air mass factor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the lat/lon matrices at the tie points:\n",
    "tieGeo=Dataset(os.path.join(pathL1,'tie_geo_coordinates.nc'),'r')\n",
    "latTie = tieGeo.variables['latitude'][:,:]\n",
    "lonTie = tieGeo.variables['longitude'][:,:]\n",
    "\n",
    "# Read the SZA and OZA matrices at the tie points:\n",
    "sunSenTie = Dataset(os.path.join(pathL1,'tie_geometries.nc'),'r')\n",
    "sunSenSor = ['sza','oza']\n",
    "\n",
    "# Perform the interpolation to fill in the gaps\n",
    "for geo in sunSenSor:\n",
    "    geoTie = sunSenTie.variables[geo.upper()][:,:]\n",
    "    sunSenInterp2 = griddata(np.transpose((latTie.ravel(),lonTie.ravel())),geoTie.ravel(),np.transpose((lat.ravel(),lon.ravel())),method='linear')\n",
    "    # Store in our caché \"im\":\n",
    "    im[geo] = np.reshape(sunSenInterp2,np.shape(im['lat']))\n",
    "\n",
    "# Compute air mass factor and store in caché\n",
    "mu = 1/np.cos(np.deg2rad(im['sza'])) + 1/np.cos(np.deg2rad(im['oza']))\n",
    "im['mu'] = mu\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### OPTIONAL: Plot SZA, OZA and the AIR MASS FACTOR in row-col geometry (without geo-referencing).\n",
    "\n",
    "i)   Analyze your findings.\n",
    "\n",
    "ii)  Why is the OZA (quasi-)independent on the row number?\n",
    "\n",
    "Hint: https://sentinel.esa.int/web/sentinel/user-guides/sentinel-3-olci/coverage\n",
    "\n",
    "iii) What is the total FOV (field-of-view) covered by your selected ROI?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer...\n",
    "# plt.imshow(im['oza'])\n",
    "# plt.colorbar()\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute BLRs and perform transmittance correction factor\n",
    "\n",
    "As you have seen in the first cells of this file, the BLRs are computed from Rayleigh-corrected reflectances. The main hypothesis of this algorithm is that the dependence of BLRs with the atmosphere can be represented by a global transmittance factor that depends only on the air mass factor, $\\mu$:\n",
    "\n",
    "$BLR(\\rho_{RC}) \\approx t_{BLR}(\\mu)BLR(\\rho_{w})$\n",
    "\n",
    "This factor was computed using radiative tranfer simulations and is can be approximated as:\n",
    "\n",
    "$t_{BLR}(\\mu) = slope(\\mu-2) + offset$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"blrTransmittance.png\" style=\"width:800px;height:500px;\">\n",
    "\n",
    "<caption><center> <u>Figure 4 [Fig. 4 G19]</u>: Equivalent transmittance ($t_{BLR}$) and bias vs. air mass factor, $\\mu=\\frac{1}{cos(\\theta_{S})}+\\frac{1}{cos(\\theta_{O})}$, for each of the BLRs used in this work. Turquoise (violet) dots represent subsets of simulations corresponding to different sun-view geometries with (without) direct sunglint. Dashed lines represent linear regressions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute BLRs\n",
    "\n",
    "blrNum   = len(blrBands)-2 # 3\n",
    "blrWave  = [olciWave[b] for b in blrBands] # wavelengths of the corresponding bands\n",
    "\n",
    "blrImg      = np.zeros((im['ROI']['R'],im['ROI']['C'],blrNum)) # Initialize BLR matrix\n",
    "blrTGainImg = np.zeros((im['ROI']['R'],im['ROI']['C'],blrNum)) # Initialize BLR matrix, corrected with transmittance factor\n",
    "\n",
    "ldL = np.zeros((blrNum,1)) # Wavlength ratios, defined below...\n",
    "ldM = np.zeros((blrNum,1))\n",
    "ldR = np.zeros((blrNum,1))\n",
    "\n",
    "for blr in range(blrNum):\n",
    "    ldL[blr] = (blrWave[1 + blr]-blrWave[2 + blr])/(blrWave[2 + blr]-blrWave[0 + blr])\n",
    "    ldM[blr] = 1\n",
    "    ldR[blr] = (blrWave[1 + blr]-blrWave[0 + blr])/(blrWave[0 + blr]-blrWave[2 + blr])\n",
    "\n",
    "    blrImg[:,:,blr] = ldL[blr]*im['rhos_' + str(blrWave[blr+0])] + \\\n",
    "                      ldM[blr]*im['rhos_' + str(blrWave[blr+1])] + \\\n",
    "                      ldR[blr]*im['rhos_' + str(blrWave[blr+2])]\n",
    "\n",
    "# BLR transmittances: slopes and offsets for the relation:\n",
    "# tBLR = slope*(mu-2) + offset\n",
    "blrTGainOffset = [ 0.9335, 0.9213, 0.9565]\n",
    "blrTGainSlope  = [-0.0480,-0.0510,-0.0300]\n",
    "\n",
    "for blr in range(blrNum):\n",
    "    if mode == 'tCorr':\n",
    "        G = blrTGainSlope[blr]*(im['mu'] - 2) + blrTGainOffset[blr]\n",
    "    else:\n",
    "        G = 1\n",
    "    blrTGainImg[:,:,blr] =  blrImg[:,:,blr]/G\n",
    "\n",
    "im['blrTGainImg'] = blrTGainImg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Find closest match between BLR at each pixel to BLR calibration grid\n",
    "The computed BLRs at each pixel are compared with a BLR calibration grid in order to assign to them the corresponding water reflectances at the NIR/SWIR bands 865 nm and 1016 nm. To do this, a point from the calibration grid (in BLR space) is assigned in order to minimize the distance to the BLR triplet at the corresponding pixel."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"blrCalSurface.png\" style=\"width:800px;height:500px;\">\n",
    "\n",
    "<caption><center> <u>Figure 3 [Fig. 6 G19]</u>: BLR(ρw) 3D space, formed by the three BLRs defined by the three consecutive triplets of the five OLCI bands 620, 709, 779, 865 and 1016 nm. Small dots: OLCI-A calibration dataset. Big colour-mapped dots: Calibration surface obtained from the OLCI-A calibration dataset, whose colour indicates water reflectance at 865 nm. Magenta dots are in situ data. The origin is indicated with an “X” and corresponds to “clear waters”."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This might take a bit longer to compute than other cells\n",
    "blrCal    = calDS.iloc[:,0:blrNum]\n",
    "blrImgLin = np.reshape(blrImg, [im['ROI']['R']*im['ROI']['C'],blrNum], order='F')\n",
    "\n",
    "blrNN = nearest_neighbors(blrImgLin, blrCal, nbr_neighbors=1)\n",
    "blrNN['idxs'] = [blr[0] for blr in blrNN['idxs']]\n",
    "\n",
    "\n",
    "for b in blrCorrBands.values():\n",
    "    wave = str(olciWave[b])\n",
    "    im['rhow_' + wave] = np.array(calDS['rhowDS[' + wave + ']'].iloc[blrNN['idxs']])\n",
    "    im['rhow_' + wave] = np.reshape(im['rhow_' + wave], [im['ROI']['R'],im['ROI']['C']], order='F')\n",
    "    im['rhoa_' + wave] = im['rhos_' + wave] - tRay(im['mu'],tauRay,0.51,b)*im['rhow_' + wave]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correct for anomalous $\\epsilon_{a}(865,1016)$ (restrict to range \"epsRange\") to correct for anomalous values\n",
    "NB: $\\epsilon_{a}(865,1016) := \\frac{\\rho_{a}(865)}{\\rho_{a}(1016)}$, where $\\rho_{a}$ is the aerosol reflectance\n",
    "\n",
    "This last correction is performed on $\\rho_{a}(865)$ to restrain the derived $\\epsilon_{a}(865,1016)$ inside the range of (0.85;1.25). These bounds were determined as the extreme values taken over a set of 82 different selected windows of size $15px \\times 15px$ from OLCI-A scenes of clear water regions close to Río de la Plata, Bahía Blanca, North Sea, Yellow Sea, Amazonas and North Australia. Also these bounds are consistent with what was obtained over the CNES-SOS simulations. This correction is performed by imposing the following condition on a pixel-by-pixel basis:\n",
    "\n",
    "$0.85\\rho_{a}(1016)≤\\rho_{a,new}(865)≤1.25\\rho_{a}(1016)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ... which in Python is expressed as:\n",
    "cted = blrCorrBands['cted']\n",
    "ctor = blrCorrBands['ctor']\n",
    "ctedWave = olciWave[cted]\n",
    "ctorWave = olciWave[ctor]\n",
    "rhoaCtedRange = np.array([im['rhoa_' + str(ctorWave)]*epsRange[b] for b in range(len(epsRange))]).transpose(1,2,0)\n",
    "rhoaCted      = np.min([np.max([im['rhoa_' + str(ctedWave)],rhoaCtedRange[:,:,0]],axis=0),rhoaCtedRange[:,:,1]],axis=0)\n",
    "im['rhoa_' + str(ctedWave)] = rhoaCted"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Perform simple linear extrapolation to specified bands\n",
    "In this first version of the algorithm, the aerosol signal is assumed to be linear in wavelength for the Red/NIR/SWIR spectral region."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for band in rhowBands:\n",
    "    wave = olciWave[band]\n",
    "    if band!=cted and band!=ctor:\n",
    "        im['rhoa_' + str(wave)] = (im['rhoa_' + str(ctedWave)] - im['rhoa_' + str(ctorWave)])*(wave - ctorWave)/(ctedWave - ctorWave) + im['rhoa_' + str(ctorWave)]\n",
    "    # store in caché\n",
    "    im['rhow_' + str(wave)] = (im['rhos_' + str(wave)] - im['rhoa_' + str(wave)])/tRay(im['mu'],tauRay,0.51,band)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Write output to netCDF\n",
    "The function \"img2NetCDF4\" defined in the \"rn\" module is based on the steps that appear here:\n",
    "\n",
    "http://www.ceda.ac.uk/static/media/uploads/ncas-reading-2015/11_create_netcdf_python.pdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build a dictionary with the names of the variables, their values, description and units.\n",
    "\n",
    "# Latitude and Longitude\n",
    "varNc = {'lat': {'val':im['lat'],'desc':'Latitude' ,'units':'degree_north [WGS84+DEM]'},\\\n",
    "         'lon': {'val':im['lon'],'desc':'Longitude','units':'degree_east  [WGS84+DEM]'}}\n",
    "\n",
    "# All the retrieved water/aerosol reflectances:\n",
    "mag = {'rhow_':'Water','rhoa_':'Aerosol'}\n",
    "for band in rhowBands:\n",
    "    for m in mag.keys():\n",
    "        varName = m + str(olciWave[band])\n",
    "        varNc[varName] = {'val':im[varName],'desc': mag[m] + ' reflectance at ' + str(olciWave[band]) + ' (OLCI band ' + str(band+1) + ')','units':'dimensionless'}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Store in netCDF file named \"rhoWBlr\"\n",
    "rn.img2NetCDF4('rhoWBlr',pathRC,varNc,'Water and aerosol reflectances retrieved by BLR-AC scheme')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
